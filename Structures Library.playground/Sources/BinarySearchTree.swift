import Foundation

public class BinarySearchTree<K:Comparable> {
    
    public private(set) var root: Node<K>?
    
    
    public init() {
           self.root = nil
    }
    
    public init(_ root: Node<K>?) {
        self.root = root
    }
        
    public func insert(_ value: K) {
        
        let node = Node(value)
        
        guard let _ = root else {
            self.root = node
            return
        }
        
        self.insertRecursive(root: self.root!, newNode: node)
    }
    
    
    private func insertRecursive(root: Node<K>, newNode: Node<K>) {
        
        if(root.key > newNode.key) {
            if let leftNode = root.left { // if it has a left child, goes down to the next level
                insertRecursive(root: leftNode, newNode: newNode)
            } else {
                root.setNode(.left, newNode) // else, means that i've found the correct position of new node
            }
        } else {
            if let rightNode = root.right {
                insertRecursive(root: rightNode, newNode: newNode)
            } else {
                root.setNode(.right, newNode)
            }
        }
    }
    
    public func search(_ element: K) -> Bool {
        guard let rootNode = self.root else { return false }
        return self.searchRecursive(rootNode,element)
    }
    
    
 
    private func searchRecursive(_ root: Node<K>, _ key: K) -> Bool {
        
        if(root.key == key) {
            return true
        }
        
        if root.key > key {
            if let leftNode = root.left {
                return searchRecursive(leftNode, key)
            }
        } else {
            if let rightNode = root.right {
                return searchRecursive(rightNode, key)
            }
        }
       
        return false
    }
    
    
    public func remove(_ element: K) -> Node<K>?  {
        return removeRecursive(self.root, element)
    }
    
 
    
    private func removeRecursive(_ root: Node<K>?, _ key: K) -> Node<K>? {
        
        guard var rootNode = root else {
            return nil
        }
        
        if rootNode.key > key {
            let left = removeRecursive(rootNode.left, key)
            root?.setNode(.left, left)
        }
        else if(rootNode.key < key) {
            let right = removeRecursive(rootNode.right, key)
            root?.setNode(.right, right)
        }
        else {
            guard let right = rootNode.right else {
                return rootNode.left
            }
            
            guard let left = rootNode.left else {
                return rootNode.right
            }
            
            rootNode = min(node: right)
            rootNode.setNode(.right, deleteMin(node: right))
            rootNode.setNode(.left, left)
        }
        
        return rootNode
    }
    
    
    
    private func min(node: Node<K>) -> Node<K> {
        
        guard let leftNode = node.left else {
            return node
        }
        
        return min(node: leftNode)
    }
    
    
    
    private func deleteMin(node: Node<K>) -> Node<K>? {
        
        guard let leftNode = node.left else {
            return node.right
        }
        
        node.setNode(.left, deleteMin(node: leftNode))
        //node.left = deleteMin(node: leftNode)
        
        return node
    }
    
    
    
    
    public func postOrder() {
        print("\nBST: [", terminator: " ")
        self.postOrderRic(root: self.root)
        print("]\n", terminator: "")
    }
    
    
    private func postOrderRic(root: Node<K>?) {
        
        guard let rootNode = root else {
            return
        }
        
        print("\(rootNode.key)", terminator: " ")
        postOrderRic(root: rootNode.left)
        postOrderRic(root: rootNode.right)
    }
    
    
    
}
