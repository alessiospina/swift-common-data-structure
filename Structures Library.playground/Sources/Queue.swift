import Foundation


public class Queue<V> {
    
    private var array: [V]
    private var debugPrint: Bool
    
    public var count: Int {
        return array.count
    }
    
    public var isEmpty : Bool {
        return array.isEmpty
    }
    
    public init() {
        self.array = []
        self.debugPrint = false
    }
    
    public init(debugPrint:Bool) {
        self.array = []
        self.debugPrint = debugPrint
    }
    
    public func enQueue(_ element: V) {
        if(debugPrint){
            print("[Queue] enqueue: \(element)")
        }
        
        self.array.append(element)
    }
    
    
    public func deQueue() -> V? {
        
        var element: V? = nil
        
        if(!self.array.isEmpty) {
            element = self.array.removeFirst()
        }
        
        if(debugPrint){
            print("[Queue] deQueue: \(String(describing: element))")
        }
        
        return element
    }
    
    public func peek() -> V? {
        
        if(debugPrint){
            print("[Queue] peek: \(String(describing: self.array.first))")
        }
        
        return self.array.first
    }
}


extension Queue: CustomStringConvertible {
    
    public var description: String {
        
        let queueElements = array.map { "\($0)"}.joined(separator: " ")
        
        return "Queue: [ " + queueElements + " ]"
    }
}
