import Foundation

public class AVLTree<K:Comparable> {
    
    public private(set) var root: Node<K>?
    
    
    
    public init() {
        self.root = nil
    }
    
    public init(_ root: Node<K>) {
        self.root = root
    }
    
    
    public func insert(_ key: K) {
        self.root = insert(self.root, key)
    }
    
    
    private func insert(_ node: Node<K>?, _ key: K ) -> Node<K> {
        guard let node = node else {
            return Node(key)
        }
        
        if key < node.key {
            let left = insert(node.left, key)
            node.setNode(.left, left)
        } else {
            let right = insert(node.right, key)
            node.setNode(.right, right)
        }
        
        let balancedNode = balanced(node)
        balancedNode.height = max(balancedNode.leftHeight, balancedNode.rightHeight) + 1
        
        return balancedNode
    }
    
    
    
    
    public func postOrder() {
        print("\nAVL: [", terminator: " ")
        self.postOrderRic(root: self.root)
        print("]\n", terminator: "")
    }
    
    
    private func postOrderRic(root: Node<K>?) {
        
        guard let rootNode = root else {
            return
        }
        
        print("\(rootNode.key)", terminator: " ")
        postOrderRic(root: rootNode.left)
        postOrderRic(root: rootNode.right)
    }
    
}


// MARK: Rotations
extension AVLTree {
    
    
    private func leftRotate(_ node: Node<K>) -> Node<K> {
        let pivot = node.right!
        
        node.setNode(.right,pivot.left)
        pivot.setNode(.left, node)
        
        node.height = max(node.leftHeight, node.rightHeight) + 1
        pivot.height = max(pivot.leftHeight, pivot.rightHeight) + 1
        
        return pivot
    }
    
    
    
    
    private func rightRotate(_ node: Node<K>) -> Node<K> {
        
        let pivot = node.left!
        
        node.setNode(.left, pivot.right)
        pivot.setNode(.right, node)
        
        node.height = max(node.leftHeight, node.rightHeight) + 1
        pivot.height = max(pivot.leftHeight, pivot.rightHeight) + 1
        
        return pivot
    }
    
    
    
    
    private func rightLeftRotate(_ node: Node<K>) -> Node<K> {
        guard let right = node.right else { return node }
        node.setNode(.right, rightRotate(right))
        return leftRotate(node)
    }
    
    
    
    
    
    private func leftRightRotate(_ node: Node<K>) -> Node<K> {
        guard let left = node.left else { return node }
        node.setNode(.left, leftRotate(left))
        return rightRotate(node)
    }
    
}



// MARK: BALANCE TREE
extension AVLTree {
    
    private func balanced(_ node: Node<K>) -> Node<K> {
        
        switch node.balanceFactor {
        
        case 2:
            if let left = node.left, left.balanceFactor == -1 {
                return leftRightRotate(node)
            } else {
                return rightRotate(node)
            }
            
        case -2:
            if let right = node.right, right.balanceFactor == 1 {
                return rightLeftRotate(node)
            } else {
                return leftRotate(node)
            }
            
        default:
            return node
        
        }
    }
}


// MARK: DELETATION
extension AVLTree {
    
    public func remove(_ key: K) {
        self.root = remove(self.root, key)
    }
    
    
    private func remove(_ node: Node<K>?, _ key: K) -> Node<K>? {
        
        guard let node = node else { return nil }
        
        if key == node.key {
            
            if node.left == nil && node.right == nil {
                return nil
            }
            
            if node.left == nil {
                return node.right
            }
            
            if node.right == nil {
                return node.left
            }
            
            node.setKey(node.right!.min.key)
            node.setNode(.right, remove(node.right, key))
        }
        else if key < node.key {
            node.setNode(.left, remove(node.left, key))
        }
        else {
            node.setNode(.right, remove(node.right, key))
        }
        
        let balancedNode = balanced(node)
        balancedNode.height = max(balancedNode.leftHeight, balancedNode.rightHeight) + 1
        
        return balancedNode
    }
}


extension AVLTree {
    
    public func contains(_ element: K) -> Bool {
        guard let rootNode = self.root else { return false }
        return self.searchRecursive(rootNode,element)
    }
    
    
    
    private func searchRecursive(_ root: Node<K>, _ key: K) -> Bool {
        
        if(root.key == key) {
            return true
        }
        
        if root.key > key {
            if let leftNode = root.left {
                return searchRecursive(leftNode, key)
            }
        } else {
            if let rightNode = root.right {
                return searchRecursive(rightNode, key)
            }
        }
        
        return false
    }
}
