import Foundation

public class Stack<V> {
    
    private var array: [V] = []
    public var debugPrint = false;
    
    public var isEmpty : Bool {
        return (array.isEmpty)
    }
    
    public var count: Int {
        return array.count
    }
    
    public init() {}
    
    public init(debugPrint: Bool) {
        self.debugPrint = debugPrint
    }
    
    public func push(_ element: V) {
        
        if(debugPrint) {
            print("[Stack] push: \(element)")
        }
        self.array.append(element)
    }
    
    public func pop() -> V? {
        
        let element = self.array.popLast()
        
        if(debugPrint) {
            print("[Stack] pop: \(String(describing: element))")
        }
        
        return element
    }
    
    public func peek() -> V? {
        
        let element = self.array.last

        if(debugPrint) {
            print("[Stack] peek: \(String(describing: element))")
        }
        
        return element
    }
    
}


extension Stack: CustomStringConvertible {
    
    public var description: String {
        
        let stackElements = array.map { "\($0)"}.reversed().joined(separator: " ")
        
        return "Stack: [ " + stackElements + " ]"
    }
}
