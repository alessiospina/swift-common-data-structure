import Foundation

public class OrderedLinearDict<K:Comparable & Hashable,V> {
    
    private var orderedKeys: [K]
    private var dict: [K:V]
    
    public init() {
        self.orderedKeys = []
        self.dict = [:]
    }
    
    
    public func put(key: K, value: V) {
        self.orderedInsertion(newKey: key)
        dict[key] = value
    }
    
    public func get(key: K) -> V? {
        return self.dict[key]
    }
    
    public func remove(key: K) {
        dict[key] = nil
        removeKey(key: key)
    }
    
    public func removeAll() {
        orderedKeys.removeAll()
        dict.removeAll()
    }
    
    public func printDict() {
        print("==========================")
        orderedKeys.forEach() { key in
            print("\(key),\(String(describing: dict[key]))")
        }
    }
    
    private func orderedInsertion(newKey: K) {
        if let index = orderedKeys.firstIndex(where: {$0 > newKey}) {
            orderedKeys.insert(newKey, at: index)
        } else {
            orderedKeys.append(newKey)
        }
    }
    
    private func removeKey(key: K) {
        if let index = orderedKeys.firstIndex(where: {$0 == key}) {
            orderedKeys.remove(at: index)
        }
    }
}

