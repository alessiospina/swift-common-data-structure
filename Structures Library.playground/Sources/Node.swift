import Foundation

public class Node<K> {
    
    public enum TypeNode { case left; case right }
    
    public private(set) var key: K
    public private(set) var left: Node?
    public private(set) var right: Node?
    
    // balance factor
    public var height = 0
    
    public var balanceFactor: Int { return leftHeight - rightHeight }
    public var leftHeight: Int { return left?.height ?? -1 }
    public var rightHeight: Int { return right?.height ?? -1 }
    
    public var min: Node {
        return left?.min ?? self
    }
    
    public init(_ key: K) {
        self.key = key
    }
    
    public func setNode(_ type: TypeNode, _ node: Node<K>?) {
        switch type {
        case .left:
            self.left = node
        case .right:
            self.right = node
        }
    }
    
    public func setKey(_ key: K) {
        self.key = key
    }
}
