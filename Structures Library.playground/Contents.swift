import Foundation


// ===== BinarySearchTree Example ====
var bst = BinarySearchTree<Int>()

bst.insert(10)
bst.insert(9)
bst.insert(8)
bst.insert(7)
bst.insert(6)

bst.postOrder()

bst.remove(6)


// ===== AVL Example ====
var avl = AVLTree<Int>()
avl.insert(10)
avl.insert(9)
avl.insert(8)
avl.insert(7)
avl.insert(6)
avl.postOrder()

avl.contains(4)
avl.remove(10)


// ===== Stack Example ====
var stack = Stack<Int>()
stack.push(10)
stack.push(11)
stack.push(12)

print(stack)

stack.isEmpty
stack.peek()
stack.pop()



// ===== Queue Example ====
var queue = Queue<Int>()
queue.enQueue(10)
queue.enQueue(11)
queue.enQueue(12)

print(queue)

queue.isEmpty
queue.peek()
queue.deQueue()


// Ordered Linear Dict Example

var dict : OrderedLinearDict<Int,[String]> = OrderedLinearDict()

dict.put(key: 10, value: ["ciao", "bello"])
dict.put(key: 20, value: ["ciao"])
dict.put(key: 1, value: ["ciao"])
dict.put(key: 1000, value: ["ciao"])

dict.printDict()

dict.get(key: 10)

dict.removeAll()
