I created this playground to contain all the data structures that I use the most.
The structures present are:
- Stack
- Queue
- Binary Search Tree
- AVL Tree
- Ordered Linear Dict

In the main file of the playground, there are some examples of use.
Class implementations can be found in Source.

